window.onscroll = () => this.menu();

function menu() {
  if (document.documentElement.scrollTop > 80) {
    document.getElementById("menu").className = "menuF";
  } else {
    document.getElementById("menu").className = "menuR";
  }
}
